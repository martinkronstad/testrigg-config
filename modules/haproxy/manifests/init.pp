class haproxy {
  package { 'haproxy':
    ensure => 'present'
  }
  service { 'haproxy':
    ensure => 'running'
  }


  $haproxy_nodes = { webA => "10.0.1.112",webB => "10.0.1.113",webC => "10.0.1.202" }
  file { '/etc/haproxy/haproxy.cfg':
    notify  => Service['haproxy'],
    content => template('haproxy/haproxy.cfg.erb'),
    owner   => root,
    group   => root,
    mode    => 644
  }
}