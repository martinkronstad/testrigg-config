 class nginx {
   package { 'nginx':
     ensure => 'installed'
   }
   service { 'nginx':
     ensure => 'running'
   }
   file { '/var/www/html/index.html':
     content => template('nginx/index.html.erb'),
     owner   => root,
     group   => root,
     mode    => 644

   }
 }