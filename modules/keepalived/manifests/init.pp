
class keepalived {
  package { 'keepalived':
    ensure => 'installed'
  }

  service { 'keepalived':
    ensure => 'running'
  }

  if $hostname =~ /^nginx(\d+)/ {
    $keepalived_shared_ip = "10.0.1.200"
    $keepalived_service = 'nginx'
    $keepalived_router_id = '51'
    if $hostname == 'nginx01' {
      $keepalived_priority = "101"
      $keepalived_state = "MASTER"
    }
    else {
      $keepalived_priority = "100"
      $keepalived_state = "BACKUP"
    }
  }

  if $hostname =~ /^haproxy(\d+)/ {
    $keepalived_shared_ip = "10.0.1.201"
    $keepalived_service = 'haproxy'
    $keepalived_router_id = '52'
    if $hostname == 'haproxy01' {
      $keepalived_priority = "101"
      $keepalived_state = "MASTER"
    }
    else {
      $keepalived_priority = "100"
      $keepalived_state = "BACKUP"
    }
  }

  if $hostname =~ /^kibana(\d+)/ {
    $keepalived_shared_ip = "10.0.1.202"
    $keepalived_service = 'kibana'
    $keepalived_router_id = '53'
    if $hostname == 'kibana01' {
      $keepalived_priority = "101"
      $keepalived_state = "MASTER"
    }
    else {
      $keepalived_priority = "100"
      $keepalived_state = "BACKUP"
    }
  }


  file { '/etc/keepalived/keepalived.conf':
    notify  => Service['keepalived'],
    content => template('keepalived/keepalived.conf.erb'),
    owner   => root,
    group   => root,
    mode    => 644

  }
}