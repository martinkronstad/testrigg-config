class wpsites {

  $sites = [
      {
        'domain' => 'mintestsitemars.no',
        'aliases' => 'www.mintestsitemars.no',
        'package' => 'mintestsitemars.no'
      }
  ]

  #$sites = ['respo.no','golfbanen.no']

  $sites.each |Integer $index, Hash $site| {
    notice("${index} = ${site['domain']}")
    if $site['package'] != "" {
      $package_name = $site['package']
    }
    else {
      $package_name = $site['domain']
    }

    package { $package_name :
      ensure => 'latest',
      install_options => ['--force-yes'],
    }
  }

}

