class developer_users {

  user { 'martin':
    ensure => present,
    comment => 'Martin Kronstad',
    home => '/home/martin',
    managehome => true
  }

  ssh_authorized_key { 'martin@Martin-Kronstads-Macbook-Pro':
    user => 'martin',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAABIwAAAQEA1UvRG37vLPuh/Fqzyuh5IeORKPk1T/T0SBfFWLp39IQhM9Ob+OQRb9wr6KrYyrSk6fcfUlWRQz2c5i8JRsA4ONLbJgtjsmxd7CivYARwPyMwdLPCq2FtarCFyDmGQD6SSo2ilnDaLLLTS9PEmZQ1yLLh6XAWU91v2TTLL4vPs2iXyVJeKxHSDm1ADTJzszB6ncXi0NPwkMcYencSNU67VE81yuw3peU89PHFtnD10GjTXuilNM3jMxu/zGsizB8GolfCAOVnztLv2Fxmp10ueIlqkElXqtxngp4um7d3n1oQKUhmz/KWSFVz1BcavqE7+Y0Z7BuggKain6x0O88WCw==',
  }
}