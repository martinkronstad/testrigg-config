stage { "init": before  => Stage["main"] }

node default {
  include cron-puppet
}


node /^kibana\d+/ {

  include cron-puppet
  include keepalived

  class { '::kibana4':
    version           => '4.5.0-linux-x64',
    install_method    => 'archive',
    archive_symlink   => true,
    manage_user       => true,
    kibana4_user      => kibana4,
    kibana4_group     => kibana4,
    kibana4_gid       => 200,
    kibana4_uid       => 200,
   plugins => {
          'elasticsearch/marvel' => {
             kibana4_plugin_dir => '/opt/kibana/installedPlugins', #optional - this is the default
             plugin_dest_dir    => 'marvel',                       #mandatory - plugin will be installed in ${kibana4_plugin_dir}/${plugin_dest_dir}
          #   url                => http://your_custom_url,         #necessary if using arbitrary URL
             ensure             => present,                        #mandatory - either 'present' or 'absent'
          },
          'elastic/sense' => {
             ensure          => present,
             plugin_dest_dir => 'sense',
          }
        },
    config            => {
        'server.port'           => 5601,
        'server.host'           => '0.0.0.0',
        'elasticsearch.url'     => 'http://10.0.1.116:9200',
        }
  }

}

node /^logstash\d+/ {

class { 'logstash':
  package_url => 'https://download.elastic.co/logstash/logstash/packages/debian/logstash_2.3.0-1_all.deb',
  autoupgrade => true,
  java_install => true
}

logstash::configfile { 'configname':
  content => template('logstash/logstash.conf')
}

}

node /^elasticsearch\d+/ {

  if $::ipaddress == '10.0.1.116' {
    $hosts = ['10.0.1.117','10.0.1.118']
  }
  elsif $::ipaddress == '10.0.1.117' {
    $hosts = ['10.0.1.116','10.0.1.118']
  }
  elsif $::ipaddress == '10.0.1.118' {
    $hosts = ['10.0.1.116','10.0.1.117']
  }

  class { 'elasticsearch':
    package_url       => 'https://download.elasticsearch.org/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.3.0/elasticsearch-2.3.0.deb',
    autoupgrade => true,
    java_install => true,
    config                   => {
      'network.host'              => $::ipaddress,
      'gateway.recover_after_nodes' => '2',
      'discovery.zen.minimum_master_nodes' => '2',
      'discovery.zen.ping.multicast.enabled' => 'false',
      'discovery.zen.ping.unicast.hosts' => $hosts,
    }
  }

  elasticsearch::instance { 'es': }
  elasticsearch::plugin{'marvel-agent': module_dir => 'marvel', instances => 'es'}
  elasticsearch::plugin{'license': module_dir => 'license', instances => 'es'}

  service { 'elasticsearch':
    ensure => 'running'
  }
}

node /^nginx\d+/ {
  include cron-puppet
  include nginx
  include keepalived

  class { 'filebeat':
    outputs => {
      'logstash'     => {
       'hosts' => [
         '10.0.1.119:5044',
         '10.0.1.205:5044'
       ],
       'loadbalance' => true,
      },
    },
  }

  filebeat::prospector { 'syslogs':
    paths    => [
      '/var/log/auth.log',
      '/var/log/syslog',
    ],
    log_type => 'syslog-beat',
  }
}

node /^apache\d+/ {
  include cron-puppet
  class { 'apache':
    mpm_module => 'prefork',
  }
  include apache::mod::php
  # Add new default page
  file { '/var/www/html/index.php':
    content => template('apache/index.php.erb'),
    owner   => root,
    group   => root,
    mode    => 644
  }
  file { '/var/www/html/check.php':
    content => template('apache/check.php.erb'),
    owner   => root,
    group   => root,
    mode    => 644
  }
  # Remove default html file
  file { "/var/www/html/index.html":
    ensure  => absent,
  }

  class { 'filebeat':
    outputs => {
      'logstash'     => {
       'hosts' => [
         '10.0.1.119:5044',
         '10.0.1.205:5044'
       ],
       'loadbalance' => true,
      },
    },
  }

  filebeat::prospector { 'syslogs':
    paths    => [
      '/var/log/auth.log',
      '/var/log/syslog',
    ],
    log_type => 'syslog-beat',
  }

}


node vvv {

  notice("DEV CONFIG");

  class { 'apt':
    update => {
      frequency => 'always',
    },
    stage => init
  }
  include wpsites
  include developer_users
}

node /^haproxy\d+/ {
  include haproxy
  include keepalived
  class { 'filebeat':
    outputs => {
      'logstash'     => {
       'hosts' => [
         '10.0.1.119:5044',
         '10.0.1.205:5044'
       ],
       'loadbalance' => true,
      },
    },
  }

  filebeat::prospector { 'syslogs':
    paths    => [
      '/var/log/auth.log',
      '/var/log/syslog',
    ],
    log_type => 'syslog-beat',
  }  

}
